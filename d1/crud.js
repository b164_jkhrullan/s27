const http = require('http');

const port = 6000;

let directory = [
	{
		"name": "Brandon",
		"email": "brandon@mail.com"
	},
	{
		"name": "Jobert",
		"email": "jobert@mail.com"
	}
]

console.log(typeof directory)


const server = http.createServer((req, res) => {

	//route for returning ALL items upon receiving a GET request
	if(req.url == '/users' && req.method == "GET"){
		res.writeHead(200, {'Content-Type': 'application/json'});
		res.write(JSON.stringify(directory));
		res.end()
	}

	if(req.url == '/users' && req.method == "POST"){

		let requestBody = ''

		req.on('data', function(data){
			requestBody += data;
		});

		req.on('end', function(){
			console.log(typeof requestBody)
			requestBody = JSON.parse(requestBody)

			let newUsers = {
				"name": requestBody.name,
				"email": requestBody.email
			}


			directory.push(newUsers);
			console.log(directory)

			res.writeHead(200, {'Content-Type': 'application/json'});
			res.write(JSON.stringify(newUsers));
			res.end()
		})

	}





















});

server.listen(port);

console.log(`Server running at localhost:${port}`);
